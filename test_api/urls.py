from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    (r'^get_user/(?P<user_id>[0-9]{1,4})/', 'rest_api.views.get_user'),
    (r'^get_users/', 'rest_api.views.get_users'),

    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
