
from django.core.serializers import serialize
from django.http import HttpResponse

def jsonify(objects):
    return HttpResponse(
        serialize('json', objects, use_natural_keys=True), 
        content_type="application/json"
    )