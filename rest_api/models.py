from django.db import models

# Create your models here.

class Departments(models.Model):

    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

    def natural_key(self):
        return self.name

class Employees(models.Model):
    department = models.ForeignKey(Departments)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)