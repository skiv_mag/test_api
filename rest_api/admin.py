from django.contrib import admin

# Register your models here.

from rest_api.models import Departments, Employees


class DepartmentAdmin(admin.ModelAdmin):
    list_display = ['name']


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'department']

admin.site.register(Departments, DepartmentAdmin)
admin.site.register(Employees, EmployeeAdmin)
