

from models import Departments, Employees
from utils import jsonify
# Create your views here.


def get_users(request):
    e_list = Employees.objects.all()
    return jsonify(e_list)


def get_user(request, user_id):
    e_list = Employees.objects.filter(pk=user_id).all()
    return jsonify(e_list)